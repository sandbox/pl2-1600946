(function ($) {

Drupal.behaviors.fboauth_publish = Drupal.behaviors.fboauth_publish || {};

Drupal.behaviors.fboauth_publish.attach = function(context) {
  $('#edit-fboauth-publish-post-as').each(function() {
    $(this).change(function() {
      if (this.value != 'me') {
        $('#edit-fboauth-publish-post-to')
	  .val(this.value)
	  .attr('disabled', 1);
      }
      else {
        $('#edit-fboauth-publish-post-to')
	  .attr('disabled', 0);
      }
    });
  });
};

})(jQuery);
