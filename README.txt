Description
===========

This module allows the user to post a link to a Facebook wall for the node
been created or updated.

Usage
=====

Make sure you have installed and configured correctly the module fboauth. You
will need to create a FB application for the current site and enter its ID in
fboauth config page. See fboauth module's README file for more info.

After fboauth is properly configured, make sure fboauth_tokens is installed.
Then go to /user/YOUR_UID/fboauth to grant access to the application.

There is a settings form at admin/config/content/fboauth-publish which allows
the user to select the content types that can be posted to FB.

The node form for the content types that you have selected should show a set of
fields to post the content to a Facebook wall.

The fields are:
* "Publish this in a Facebook Wall": check this if you want to post this content.
    It is enabled by default for new nodes and disabled when you edit an
    existing node.
 
* "Account to post from": The Facebook identity you want to use to publish.
    The default account will be the one that you used to authorize the app.
    The rest of the accounts in the list will be pages and apps that you are
    administrator of.
    You can post as any of those accounts but restrictions apply to where
    you can post with each of them. See below.

* "Page or wall to post to": The Facebook wall you want to publish to.
    The behaviour of this field depends on what you select in the previous
    field ("post from").
    If you selected your main account as "post from" account, then you can
    post to any of the available FB walls.
    If you selected in the "post from" field any of the other accounts, then
    you can only select to the wall for that account page.

* "Message": The message you want to attach to the link.

Saving the node should post its url to the selected FB wall.

FB will grab the first image from the page to show on the wall with the link.
This module doesn't provide (yet) a way to specify which image you want to show next
to your link.
